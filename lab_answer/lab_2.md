###Apakah perbedaan antara JSON dan XML?
- XML(Extensible Markup Language) adalah bahasa markup yang lebih berorientasi kepada dokumen, tetapi digunakan untuk representasi struktur data yang berubah-ubah 
- JSON(JavaScript Object Notation) adalah format teks terustruktur yang berdasarkan bahasa pemrograman JavaScript

####Perbedaan:
- Syntax JSON lebih pendek dibanding dengan XML
- JSON hanya mendukung objek tipe data primitif, seperti _integer_, _string_,_boolean_, dll. Sedangkan XML mendukung tipa data kompleks seperti bagan, charts, table, dll.
- JSON mendukung arrays. XML tidak
- XML harus di-_parse_ menggunakan XML Parser. JSON bisa di-_parse_ menggunakan fungsi JavaScript standar


###Apakah perbedaan antara HTML dan XML?
- HTML(Hyper Text Markup Language) adalah bahasa markup yang digunakan untuk mendeskripsikan struktur dari laman web

####Perbedaan:
- HTML tags dapat digunakan untuk menampilkan data. XML hanya digunakan untuk mendeskripsikan dan menyimpan data, bukan menampilkan/_display_
- HTML bersifat statis, XML dinamis
- HTML tidak case sensitive. XML case sensitive
- Tags pada HTML sudah predefined. Tags pada XML dapat kita define sendiri
- 

####Sumber: 
https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://www.imaginarycloud.com/blog/json-vs-xml/
https://www.w3schools.com/js/js_json_xml.asp
https://www.geeksforgeeks.org/html-vs-xml/