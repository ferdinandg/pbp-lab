
from django import forms
from lab_2.models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = "__all__"

        widgets = {
            'note_to': forms.TextInput(attrs={'class': 'form-control'}),
            'note_from': forms.TextInput(attrs={'class': 'form-control'}),
            'note_title': forms.TextInput(attrs={'class': 'form-control'}),
            'note_message': forms.Textarea(attrs={'class': 'form-control'})
        }
