import 'dart:html';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home:  ToDoForm()
    );
  }
  
}

class ToDoForm extends StatefulWidget {
  @override
  _ToDoFormState createState() => _ToDoFormState();
}

class _ToDoFormState extends State<ToDoForm> {
  final _formKey = GlobalKey<FormState>();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
          backgroundColor: Color(0xffCCCCFF),
          appBar: AppBar(
            title: Text("To-Do List")
          ),
          body: Form(
            key: _formKey,
            child: Center(
              child: Container(
                padding: EdgeInsets.all(10.0),
                child: Column(
              children: [
                Text(
                  'Things You Can Do During Quarantine',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 32)
                  ),
                Text(
                  'Share with others here', 
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 24)
                  ),
                Container(height: 24),
                TextFormField(
                decoration: new InputDecoration(
                  hintText: "To-do",
                  border: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(5.0)),
                  ),
                  validator: (value) {
                    if (value!.isEmpty) {
                        return 'To-do tidak boleh kosong!';
                      }
                      return null;
                  },
                ),
                Container(height: 40),
                OutlinedButton(
                  onPressed: () {
                    print('Received click');
                    if (_formKey.currentState!.validate()) {}
                  },
                  child: const Text('Send!'),
                ),
                
                Container(height: 40),
                Card(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      const ListTile(
                      title: Text('Tidur Seharian :D'),
                      ),
                    ],
                  ),
                ),
                Container(height: 10),
                Card(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      const ListTile(
                      title: Text('Ngeteh'),
                      ),
                    ],
                  ),
                ),
              ],
            ) 
              ),
           )
            
          ),
      
            
          );
    
  }
}

// class extends StatelessWidget {
  
//   const MyAppState({Key? key, required this.title}) : super(key: key);
//   final String title;
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//           backgroundColor: Color(0xffCCCCFF),
//           appBar: AppBar(
//             title: Text("To-Do List")
//           ),
//           body: Center(
//             child: Column(
//               children: [
//                 Text(
//                   'Things You Can Do During Quarantine',
//                   textAlign: TextAlign.center,
//                   style: TextStyle(fontSize: 32)
//                   ),
//                 Text(
//                   'Share with others here', 
//                   textAlign: TextAlign.center,
//                   style: TextStyle(fontSize: 24)
//                   ),
//                 Container(height: 24),
//                 TextFormField(
//                 decoration: new InputDecoration(
//                   hintText: "To-do",
//                   border: OutlineInputBorder(
//                     borderRadius: new BorderRadius.circular(5.0)),
//                   ),  
//                 ),
//                 Container(height: 40),
//                 OutlinedButton(
//                   onPressed: () {
//                     print('Received click');
//                   },
//                   child: const Text('Send!'),
//                 ),
                
//                 Container(height: 40),
//                 Card(
//                   child: Column(
//                     mainAxisSize: MainAxisSize.min,
//                     children: <Widget>[
//                       const ListTile(
//                       title: Text('Tidur Seharian :D'),
//                       ),
//                     ],
//                   ),
//                 ),
//                 Container(height: 10),
//                 Card(
//                   child: Column(
//                     mainAxisSize: MainAxisSize.min,
//                     children: <Widget>[
//                       const ListTile(
//                       title: Text('Ngeteh'),
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             )
//            )
//           );
    
//   }

// }




