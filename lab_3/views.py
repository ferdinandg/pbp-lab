from django.shortcuts import render
from django.http.response import HttpResponseRedirect
from django.contrib.auth.decorators import  login_required
from lab_1.models import Friend
from .forms import FriendForm


# Create your views here.
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends':friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login')
def add_friend(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3')
    else: # if not POST, create blank form
        form = FriendForm()

    return render(request, 'lab3_form.html', {'form': form})